function SinhVien(
    _ma,
    _ten,
    _email,
    _pass,
    _ngaySinh,
    _khoaHoc,
    _diemToan,
    _diemLy,
    _diemHoa
){
    this.ma = _ma;
    this.ten = _ten;
    this.email = _email;
    this.pass = _pass;
    this.ngaySinh = _ngaySinh;
    this.khoaHoc = _khoaHoc;
    this.diemToan = _diemToan;
    this.diemLy = _diemLy;
    this.diemHoa = _diemHoa;
    this.tinhDTB = function(){
        return (this.diemHoa + this.diemLy + this.diemToan) / 3;
    }
}