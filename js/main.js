var dssv = [];

var dssvJSON = localStorage.getItem("dssv");

// Trong TH localStorage trống thì bỏ qua bước này và chạy luôn function themSinhVien() bên dưới
if(dssvJSON != null){
    var svArr = JSON.parse(dssvJSON);
    // convert array chứa object ko có key tinhDTB() thành array chứa object có key tinhDTB()
    dssv = svArr.map(function(item){
        return new SinhVien(item.ma, item.ten, item.email, item.pass,item.ngaySinh, item.khoaHoc, item.diemToan, item.diemLy, item.diemHoa);
    })
    renderLayout(dssv);
}

// Thêm sinh viên
function themSinhVien(){
    var sv = layThongTinTuForm();

    // validate
    var isValue = validateSV(sv);

    if(isValue){
        dssv.push(sv);
        // Đẩy data lên localStorage
        var dssvJSON = JSON.stringify(dssv);
        localStorage.setItem("dssv",dssvJSON);

        // Hiển thị thông tin trên layout
        renderLayout(dssv);
    }
}

// Xóa sinh viên
function xoaSinhVien(idSV){
    // Tìm vị trí index
    var viTri = timViTriIndex(idSV,dssv);

    // Xóa sinh viên
    dssv.splice(viTri,1);
    var dssvJSON = JSON.stringify(dssv);
    localStorage.setItem("dssv",dssvJSON);
    renderLayout(dssv);
}

// Sửa sinh viên
function suaSinhVien(idSV){
    // show thông tin sinh viên
    showThongTinSinhVien(idSV,dssv);
    document.getElementById("txtMaSV").setAttribute("disabled", "");
    resetThongTinSpan();
}

// Cập nhật sinh viên
function capNhatSinhVien(){
    // Cập nhật
    var sv = layThongTinTuForm();
    var viTri = timViTriIndex(sv.ma,dssv);

    var isValue = validateSVUpdate(sv);

    if(isValue){
        for (let index = 0; index < dssv.length; index++) {
            if(viTri == index){
                dssv[index] = sv;
                renderLayout(dssv);
                break;
            }
        }

        document.getElementById("txtMaSV").removeAttribute("disabled", "");
        resetThongTin();

        // Update dữ liệu trên localStorage
        var dssvJSON = JSON.stringify(dssv);
        localStorage.setItem("dssv",dssvJSON);
    }
}