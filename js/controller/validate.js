// Kiểm tra trùng
function kiemTraTrungMaSV(idSV, svArr){
    var index = svArr.findIndex(function(item){
        return idSV == item.ma;
    })
    
    if(index == -1){
        document.getElementById("spanMaSV").innerText = "";
        return true;
    } else{
        document.getElementById("spanMaSV").innerText = "Mã sinh viên đã tồn tại!";
        return false;
    }
}

function kiemTraTrungEmail(idEmail, svArr){
    var index = svArr.findIndex(function(item){
        return idEmail == item.email;
    })
    
    if(index == -1){
        document.getElementById("spanEmailSV").innerText = "";
        return true;
    } else{
        document.getElementById("spanEmailSV").innerText = "Email đã tồn tại!";
        return false;
    }
}

// Kiểm tra bỏ trống
function kiemTraBoTrong(idString, idErr){
    if(idString.length == 0){
        document.getElementById(idErr).innerHTML = `*<i>bắt buộc điền</i>`;
        return false;
    } else{
        document.getElementById(idErr).innerText = "";
        return true;
    }
}

function kiemTraBoTrongKH(idString, idErr){
    if(idString == "ckh"){
        document.getElementById(idErr).innerHTML = `*<i>bắt buộc điền</i>`;
        return false;
    } else{
        document.getElementById(idErr).innerText = "";
        return true;
    }
}

// Kiểm tra độ dài
function kiemTraDoDai(idString, idErr, min, max){
    if(idString.length < min || idString.length > max){
        document.getElementById(idErr).innerText = `Độ dài phải từ ${min} đến ${max} kí tự!`;
        return false;
    } else{
        document.getElementById(idErr).innerText = "";
        return true;
    }
}

// Kiểm tra số (mã SV + điểm)
function kiemTraSo(idString, idErr){
    var reg = /^\d+$/;
    if(reg.test(idString)){
        document.getElementById(idErr).innerText = "";
        return true;
    } else{
        document.getElementById(idErr).innerText = "Trường này phải là số!";
        return false;
    }
}

// Kiểm tra string (tên)
function kiemTraString(idString, idErr){
    var res = /^[a-zA-Z]*$/;
    if(res.test(idString)){
        document.getElementById(idErr).innerText = "";
        return true;
    } else{
        document.getElementById(idErr).innerText = "Trường này phải là chữ và không có dấu cách!";
        return false;
    }
}

// Kiểm tra điểm
function kiemTraDiem(idString, idErr, min, max){
    if(idString < min || idString > max){
        document.getElementById(idErr).innerText = `Điểm phải từ ${min} đến ${max}!`;
        return false;
    } else{
        document.getElementById(idErr).innerText = "";
        return true;
    }
}

// Kiểm tra email
function kiemTraEmail(idString, idErr){
    const re =
  /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    if(re.test(idString)){
        document.getElementById(idErr).innerText = "";
        return true;
    } else{
        document.getElementById(idErr).innerText = "Email không hợp lệ!";
        return false;
    }
}