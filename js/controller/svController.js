// Lấy thông tin từ form
function layThongTinTuForm(){
    var ma = document.getElementById("txtMaSV").value;
    var ten = document.getElementById("txtTenSV").value;
    var email = document.getElementById("txtEmail").value;
    var pass = document.getElementById("txtPass").value;
    var ngaySinh = document.getElementById("txtNgaySinh").value;
    var khoaHoc = document.getElementById("khSV").value;
    var diemToan = document.getElementById("txtDiemToan").value *1;
    var diemLy = document.getElementById("txtDiemLy").value *1;
    var diemHoa = document.getElementById("txtDiemHoa").value *1;

    var sv = new SinhVien(ma,ten,email,pass,ngaySinh,khoaHoc,diemToan,diemLy,diemHoa);

    return sv;
}

// Hiển thị thông tin trên Layout (renderLayout)
function renderLayout(svArr){
    var contentTable = "";

    for (let index = 0; index < svArr.length; index++) {
        var item = svArr[index];
        var contentHTML = `
                    <tr>
                        <th>${item.ma}</th>
                        <th>${item.ten}</th>
                        <th>${item.email}</th>
                        <th>${item.ngaySinh}</th>
                        <th>${item.khoaHoc}</th>
                        <th>${item.tinhDTB()}</th>
                        <th>
                            <button onclick="xoaSinhVien('${item.ma}')" class="btn btn-danger">Xóa</button>
                            <button onclick="suaSinhVien('${item.ma}')" class="btn btn-warning">Sửa</button>
                        </th>
                    </tr>
        `
        contentTable += contentHTML;
    }

    document.getElementById("tbodySinhVien").innerHTML= contentTable;
};

// Tìm vị trí index
function timViTriIndex(idSV,dssv){
    var viTri = -1
    for (let index = 0; index < dssv.length; index++) {
        var objectDSSV = dssv[index];
        if(objectDSSV.ma == idSV){
            viTri = index;
            break;
        }
    };
    return viTri;
}

// Show thông tin sinh viên
function showThongTinSinhVien(idSV,dssv){
    var viTri = timViTriIndex(idSV,dssv);
    var item = dssv[viTri];

    document.getElementById("txtMaSV").value = item.ma;
    document.getElementById("txtTenSV").value = item.ten;
    document.getElementById("txtEmail").value = item.email;
    document.getElementById("txtPass").value = item.pass;
    document.getElementById("txtNgaySinh").value = item.ngaySinh;
    document.getElementById("khSV").value = item.khoaHoc;
    document.getElementById("txtDiemToan").value = item.diemToan;
    document.getElementById("txtDiemLy").value = item.diemLy;
    document.getElementById("txtDiemHoa").value = item.diemHoa;
}

// Reset lại thông tin
function resetThongTin(){
    document.getElementById("txtMaSV").value = "";
    document.getElementById("txtTenSV").value = "";
    document.getElementById("txtEmail").value = "";
    document.getElementById("txtPass").value = "";
    document.getElementById("txtNgaySinh").value = "";
    document.getElementById("khSV").value = "ckh";
    document.getElementById("txtDiemToan").value = "";
    document.getElementById("txtDiemLy").value = "";
    document.getElementById("txtDiemHoa").value = "";

    document.getElementById("spanMaSV").innerText = "";
    document.getElementById("spanTenSV").innerText = "";
    document.getElementById("spanEmailSV").innerText = "";
    document.getElementById("spanMatKhau").innerText = "";
    document.getElementById("spanNgaySinh").innerText = "";
    document.getElementById("spanKhoaHoc").innerText = "";
    document.getElementById("spanToan").innerText = "";
    document.getElementById("spanLy").innerText = "";
    document.getElementById("spanHoa").innerText = "";

    document.getElementById("txtMaSV").removeAttribute("disabled", "");
}

function resetThongTinSpan(){
    document.getElementById("spanMaSV").innerText = "";
    document.getElementById("spanTenSV").innerText = "";
    document.getElementById("spanEmailSV").innerText = "";
    document.getElementById("spanMatKhau").innerText = "";
    document.getElementById("spanNgaySinh").innerText = "";
    document.getElementById("spanKhoaHoc").innerText = "";
    document.getElementById("spanToan").innerText = "";
    document.getElementById("spanLy").innerText = "";
    document.getElementById("spanHoa").innerText = "";
}

// Validate sinh viên
function validateSV(sv){
    var isValueMa = true;
    isValueMa = kiemTraBoTrong(sv.ma,"spanMaSV") && kiemTraDoDai(sv.ma,"spanMaSV",4,8) && kiemTraSo(sv.ma,"spanMaSV") && kiemTraTrungMaSV(sv.ma,dssv);

    var isValueTen = true;
    isValueTen = kiemTraBoTrong(sv.ten,"spanTenSV") && kiemTraDoDai(sv.ten, "spanTenSV", 6, 24) && kiemTraString(sv.ten,"spanTenSV");

    var isValueEmail = true;
    isValueEmail = kiemTraBoTrong(sv.email,"spanEmailSV") && kiemTraEmail(sv.email,"spanEmailSV") && kiemTraTrungEmail(sv.email,dssv);

    var isValuePass = true;
    isValuePass = kiemTraBoTrong(sv.pass,"spanMatKhau") && kiemTraDoDai(sv.pass, "spanMatKhau", 8, 20);

    var isValueNgaySinh = true;
    isValueNgaySinh = kiemTraBoTrong(sv.ngaySinh,"spanNgaySinh");

    var isValueKhoaHoc = true;
    isValueKhoaHoc = kiemTraBoTrongKH(sv.khoaHoc,"spanKhoaHoc");

    var isValueToan = true;
    isValueToan = kiemTraDiem(sv.diemToan,"spanToan",0,10) && kiemTraSo(sv.diemToan,"spanToan") ;

    var isValueLy = true;
    isValueLy = kiemTraDiem(sv.diemLy,"spanLy",0,10) && kiemTraSo(sv.diemLy,"spanLy");

    var isValueHoa = true;
    isValueHoa = kiemTraDiem(sv.diemHoa,"spanHoa",0,10) && kiemTraSo(sv.diemHoa,"spanHoa");

    var isValue = true;
    isValue = isValueMa & isValuePass & isValueEmail & isValueTen & isValueNgaySinh & isValueToan & isValueLy & isValueHoa;

    return isValue;
}

function validateSVUpdate(sv){
    var isValueMa = true;
    isValueMa = kiemTraBoTrong(sv.ma,"spanMaSV") && kiemTraDoDai(sv.ma,"spanMaSV",4,8) && kiemTraSo(sv.ma,"spanMaSV");

    var isValueTen = true;
    isValueTen = kiemTraBoTrong(sv.ten,"spanTenSV") && kiemTraDoDai(sv.ten, "spanTenSV", 6, 24) && kiemTraString(sv.ten,"spanTenSV");

    var isValueEmail = true;
    isValueEmail = kiemTraBoTrong(sv.email,"spanEmailSV") && kiemTraEmail(sv.email,"spanEmailSV");

    var isValuePass = true;
    isValuePass = kiemTraBoTrong(sv.pass,"spanMatKhau") && kiemTraDoDai(sv.pass, "spanMatKhau", 8, 20);

    var isValueNgaySinh = true;
    isValueNgaySinh = kiemTraBoTrong(sv.ngaySinh,"spanNgaySinh");

    var isValueKhoaHoc = true;
    isValueKhoaHoc = kiemTraBoTrongKH(sv.khoaHoc,"spanKhoaHoc");

    var isValueToan = true;
    isValueToan = kiemTraDiem(sv.diemToan,"spanToan",0,10) && kiemTraSo(sv.diemToan,"spanToan") ;

    var isValueLy = true;
    isValueLy = kiemTraDiem(sv.diemLy,"spanLy",0,10) && kiemTraSo(sv.diemLy,"spanLy");

    var isValueHoa = true;
    isValueHoa = kiemTraDiem(sv.diemHoa,"spanHoa",0,10) && kiemTraSo(sv.diemHoa,"spanHoa");

    var isValue = true;
    isValue = isValueMa & isValuePass & isValueEmail & isValueTen & isValueNgaySinh & isValueToan & isValueLy & isValueHoa;

    return isValue;
}